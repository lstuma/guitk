import pyglet
import guitk.rendering as rendering
from typing import Optional


def mainloop():
    # Start pyglet mainloop
    pyglet.app.run()


class GUI(object):
    def __init__(self, window=None, **kwargs):
        # lambda for checking whether certain properties are provided otherwise using standard values
        check = lambda keyword, alternative=None: kwargs[keyword] if keyword in kwargs.keys() else alternative
        # The window the GUI will be rendered in
        self.window: rendering.Window = window if window else rendering.Window()
        # List of all widgets handled by the GUI object
        self.widgets: list = list()

        # Position of mouse
        self._mouse_position = (0, 0)

        # Dictionary with all events and all buttons bound to them
        self.event_subscribers: dict = dict()

        """
            Please add events later
        """

        self.window.on_mouse_press = self.on_mouse_press
        self.window.on_mouse_release = self.on_mouse_release
        self.window.on_mouse_drag = self.on_mouse_drag
        self.window.on_mouse_motion = self.on_mouse_motion
        self.window.on_key_press = self.on_key_press
        self.window.on_key_release = self.on_key_release

    def on_mouse_press(self, x, y, button, modifiers):
        # Check if event has even been registered
        if f'button-{button}' not in self.event_subscribers.keys():
            return
        # Check which widget the mouse is hovering on
        clicked_widget = self.get_hovered_widget()
        # find the item
        for item in self.event_subscribers[f'button-{button}']:
            if item[0] == clicked_widget:
                item[1](f'button-{button}')


    def on_mouse_release(self, x, y, button, modifiers):
        pass

    def on_mouse_drag(self, x, y, dx, dy, button, modifiers):
        pass

    def on_mouse_motion(self, x, y, dx, dy):
        # Update mouse position
        self._mouse_position = (x, y)

    def on_key_press(self, symbol, modifiers):
        # Check if anything has been registered under the event
        if f'key-{symbol}' not in self.event_subscribers.keys():
            return

        # Otherwise, call all functions subscribed to the event
        for widget in self.event_subscribers[f'key-{symbol}']:
            widget[1](f'key-{symbol}')

    def on_key_release(self, symbol, modifiers):
        pass

    def get_hovered_widget(self):
        """
        :return: The widget the mouse is positioned upon
        """
        for widget in self.widgets:
            if widget.bottom_left[0] < self.mouse_position[0] < widget.top_right[0] and\
               widget.bottom_left[1] < self.mouse_position[1] < widget.top_right[1]:
                return widget

    @property
    def mouse_position(self):
        return self._mouse_position

    def add_button(self, **kwargs):
        self.widgets.append(Button(gui=self, **kwargs))
        return self.widgets[-1]

    """
        Subscribable events:
            -   button-0
            -   button-1
            -   button-2
            -   keyboard buttons (key-539 for example)
    """

    def bind(self, widget, event, function):
        # Check if event is being registered the first time
        if event not in self.event_subscribers.keys():
            self.event_subscribers[event] = list()
        # Add widget to event_subscribers list if it isn't already in there
        if (widget, function) not in self.event_subscribers[event]:
            self.event_subscribers[event].append((widget, function))

    def unbind(self, widget, event, function):
        # Check if there is a list for the event subscribers of this event otherwise return
        if event not in self.event_subscribers.keys():
            return
        # If widget in event_subscribers list remove it
        if (widget, function) in self.event_subscribers[event].values():
            self.event_subscribers[event].remove((widget, function))


class Widget(object):
    def __init__(self, **kwargs):
        # The GUI object which this widget belongs to
        self.gui = kwargs['gui']

        # Children objects like rectangles, labels, ...
        self.child_objects = list()

        # Modifiers (hovered, clicked, ....)
        self.modifiers = dict()

        # Build the widget (shape, ...)
        self.build_widget(**kwargs)

    def bind(self, event, function):
        self.gui.bind(self, event, function)

    def unbind(self, event, function):
        self.gui.unbind(self, event, function)

    def remove(self):
        # Remove widget from gui
        self.gui.widgets.remove(self)
        # Destroy all shapes and graphics objects
        for object in self.child_objects:
            object.delete()

    @property
    def top_right(self):
        return (self.position[0]+self.width, self.position[1]+self.height)

    @property
    def bottom_left(self):
        return self.position

class Button(Widget):
    def __init__(self, **kwargs):
        # Instantiate widget class
        super().__init__(**kwargs)

    def build_widget(self, **kwargs):
        # lambda for checking whether certain properties are provided otherwise using standard values
        check = lambda keyword, alternative=None: kwargs[keyword] if keyword in kwargs.keys() else alternative

        # Create background rectangle for button
        self.child_objects.append(rendering.Rectangle(position=kwargs['position'], width=kwargs['width'],
                                             height=kwargs['height'], color=check('background', (255, 255, 255)),
                                             batch=self.gui.window.batch, layer=kwargs['layer']))

        # Create text for button
        self.child_objects.append(rendering.Label(position=kwargs['position'], width=kwargs['width'], height=kwargs['height'],
                                     batch=self.gui.window.batch, layer=kwargs['layer']+1, color=check('foreground', (0, 0, 0, check('opacity', 255))),
                                     text=check('text', ''), anchor_x=check('anchor_x', 'left'),
                                     anchor_y=check('anchor_y', 'bottom'), align=check('align', 'center')))

    @property
    def center(self):
        return self.position[0]+self.width/2, self.position[1]+self.height/2

    @property
    def position(self):
        return self.child_objects[0].position

    @position.setter
    def position(self, value):
        self.child_objects[0].position = value

    @property
    def width(self):
        return self.child_objects[0].width

    @width.setter
    def width(self, value):
        self.child_objects[0].width = value

    @property
    def height(self):
        return self.child_objects[0].height