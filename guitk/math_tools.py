import math
import random

def get_distance(a: tuple, b: tuple):
    # calculates distance using the theorem of pythagoras
    distance = math.sqrt(math.pow(b[0]-a[0], 2) + math.pow(b[1]-a[1], 2))
    return distance

def normalize(a: tuple):
    # returns a vector with the same direction however having an absolute value of 1
    total = abs(a[0]) + abs(a[1])
    if total > 0:
        normalized = (a[0] / total, a[1] / total)
    else:
        normalized = normalize((random.uniform(-1, 1), random.uniform(-1, 1)))
    return normalized