import pyglet


class Window(pyglet.window.Window):

    def __init__(self, width=480, height=360, caption='Window', resizable=False):
        # Instantiate window
        super().__init__(width=width, height=height, caption=caption, resizable=resizable)
        # Batch of objects that will be drawn in the window
        self.batch = pyglet.graphics.Batch()

    def on_draw(self):
        # Clear window before drawing
        self.clear()
        # Draw the batch
        self.batch.draw()


def get_layer(layer):
    return pyglet.graphics.OrderedGroup(layer)


def run_app():
    # Run app
    pyglet.app.run()


class Shape(object):
    def __init__(self, **kwargs):
        # All properties of the circle
        self.properties = dict()

        # The actual object being rendered
        self.shape = None

        # Set properties
        self.set_properties(**kwargs)

        # Create shape
        self.build_shape(**kwargs)

    def set_properties(self, **kwargs):
        # Update all properties passed by the arguments
        for arg_key, arg_value in kwargs.items():
            self.properties[arg_key] = arg_value

    def configure(self, **kwargs):
        # Update properties
        self.set_properties(**kwargs)

        # Update shape
        for arg_key, arg_value in kwargs.items():
            # lambda for lazy writing
            match = lambda case: arg_key == case
            self.update_shape(match, arg_value)


class Circle(Shape):
    def __init__(self, **kwargs):
        # Initialize shape class
        super().__init__(**kwargs)

        # Layer the circle is being rendered on
        self._layer = kwargs['layer']

    def build_shape(self, **kwargs):
        # Create shape
        self.shape = pyglet.shapes.Circle(self.properties['position'][0], self.properties['position'][1],
                                          radius=self.properties['radius'], batch=self.properties['batch'],
                                          group=get_layer(self.properties['layer']), color=self.properties['color'])

    @property
    def position(self):
        return self.shape.position

    @position.setter
    def position(self, value):
        self.shape.position = value

    @property
    def radius(self):
        return self.shape.radius

    @radius.setter
    def radius(self, value):
        self.shape.radius = value

    @property
    def batch(self):
        return self.shape._batch

    @batch.setter
    def batch(self, value):
        self.shape._batch = value

    @property
    def layer(self):
        return self._layer

    @layer.setter
    def layer(self, value):
        self.shape._group = get_layer(value)
        self._layer = value

    @property
    def color(self):
        return self.shape.color

    @color.setter
    def color(self, value):
        self.shape.color = value


class Rectangle(Shape):
    def __init__(self, **kwargs):
        # Initialize shape class
        super().__init__(**kwargs)

        # Layer the rectangle is being rendered on
        self._layer = kwargs['layer']

    def build_shape(self, **kwargs):
        # lambda for checking whether certain properties are provided otherwise using standard values
        check = lambda keyword, alternative=None: kwargs[keyword] if kwargs[keyword] else alternative
        # Create shape
        self.shape = pyglet.shapes.Rectangle(self.properties['position'][0], self.properties['position'][1],
                                             width=self.properties['width'], height=self.properties['height'],
                                             batch=self.properties['batch'], group=get_layer(self.properties['layer']),
                                             color=check('color', (100, 100, 100)))


    @property
    def position(self):
        return self.shape.position

    @position.setter
    def position(self, value):
        self.shape.position = value

    @property
    def width(self):
        return self.shape.width

    @width.setter
    def width(self, value):
        self.shape.width = value

    @property
    def height(self):
        return self.shape.height

    @height.setter
    def height(self, value):
        self.shape.height = value

    @property
    def batch(self):
        return self.shape._batch

    @batch.setter
    def batch(self, value):
        self.shape._batch = value

    @property
    def layer(self):
        return self._layer

    @layer.setter
    def layer(self, value):
        self.shape._group = get_layer(value)
        self._layer = value

    @property
    def color(self):
        return self.shape.color

    @color.setter
    def color(self, value):
        self.shape.color = value


class Label(Shape):
    def __init__(self, **kwargs):
        # Initialize shape class
        super().__init__(**kwargs)

        # Layer the rectangle is being rendered on
        self._layer = kwargs['layer']

    def build_shape(self, **kwargs):
        # lambda for checking whether certain properties are provided otherwise using standard values
        check = lambda keyword, alternative=None: kwargs[keyword] if keyword in kwargs.keys() else alternative
        # Create shape
        self.shape = pyglet.text.Label(kwargs['text'],
                                       font_name=check('font_name'), font_size=check('font_size'),
                                       bold=check('bold', False), italic=check('italic', False),
                                       stretch=check('stretch', False),
                                       color=check('color', (255, 255, 255, 255)),
                                       x=kwargs['position'][0], y=kwargs['position'][1],
                                       width=check('width'), height=check('height'),
                                       anchor_x=check('anchor_x', 'left'), anchor_y=check('anchor_y', 'baseline'),
                                       align=check('align', 'left'),
                                       multiline=check('multiline', False), dpi=check('dpi'),
                                       batch=self.properties['batch'], group=get_layer(self.properties['layer']))

    @property
    def position(self):
        return self.shape.position

    @position.setter
    def position(self, value):
        self.shape.position = value

    @property
    def width(self):
        return self.shape.width

    @width.setter
    def width(self, value):
        self.shape.width = value

    @property
    def height(self):
        return self.shape.height

    @height.setter
    def height(self, value):
        self.shape.height = value

    @property
    def italic(self):
        return self.shape.italic

    @italic.setter
    def italic(self, value):
        self.shape.italic = value

    @property
    def bold(self):
        return self.shape.bold

    @bold.setter
    def bold(self, value):
        self.shape.bold = value

    @property
    def anchor_x(self):
        return self.shape.anchor_x

    @anchor_x.setter
    def anchor_x(self, value):
        self.shape.anchor_x = value

    @property
    def anchor_y(self):
        return self.shape.anchor_y

    @anchor_y.setter
    def anchor_y(self, value):
        self.shape.anchor_y = value

    @property
    def multiline(self):
        return self.shape.multiline

    @multiline.setter
    def multiline(self, value):
        self.shape.multiline = value

    @property
    def dpi(self):
        return self.shape.dpi

    @dpi.setter
    def dpi(self, value):
        self.shape._dpi = value

    @property
    def batch(self):
        return self.shape.batch

    @batch.setter
    def batch(self, value):
        self.shape._batch = value

    @property
    def layer(self):
        return self._layer

    @layer.setter
    def layer(self, value):
        self.shape._group = get_layer(value)
        self._layer = value

    @property
    def color(self):
        return self.shape.color

    @color.setter
    def color(self, value):
        self.shape.color = value
