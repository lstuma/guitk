import unittest
import time


class TestStringMethod(unittest.TestCase):

    def test_001_opening_window(self):
        """
            Opening a pyglet window using gui module
        """

        from guitk import gui

        # Open window
        _gui = gui
        gui.mainloop()

    def test_002_window_with_buttons(self):
        """
            Opening a window with a button and binding the 'd' key to the hello_world function using the gui module
        """

        from guitk import gui
        import guitk.rendering as rendering

        # Open window
        _gui = gui.GUI()
        # Add button
        button = _gui.add_button(position=(100, 200), width=300, height=20, layer=1, text='unittest_window_with_button', foreground=(255, 100, 50, 255))
        button.bind('key-100', self.hello_world)
        gui.mainloop()

    def test_002_window_with_buttons(self):
        """
            Opening a window with a button and binding the mouse button-4 to the hello_world function using the gui module
        """

        from guitk import gui
        import guitk.rendering as rendering

        # Open window
        _gui = gui.GUI()
        # Add button
        button = _gui.add_button(position=(100, 200), width=300, height=20, layer=1, text='unittest_window_with_button', foreground=(255, 100, 50, 255))
        button.bind('button-4', self.hello_world)
        gui.mainloop()

    def hello_world(self, event):
        print(f'{time.time()} hello world')


if __name__ == '__main__':
    unittest.main()
